# bazel

bazel 管理项目工具练习。

## bazel使用

### workspace
声明了bazel项目的根目录，项目内的文件都是相对于根目录。

### BUILD
name: 可看做 target。

hdrs: 依赖的 .h 文件。

srcs: 源文件

deps: 依赖的其他 name。
同一目录下的用 :name，不同目录的用相对根目录路径 //src/config:name

copts
编译期参数

linkopts
链接期参数

visibility
name的可见度

### Makefile
辅助工具，简化编译命令


### 学习记录
1. 每一个 cc_library 里面的 srcs 是直接cc文件，hdrs 直接.h文件，其他的依赖通过 deps 依赖 name管理。
2. linkopts 在 macOS 中编译不需要加入。
