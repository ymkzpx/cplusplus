#ifndef BANK_H
#define BANK_H
#include <iostream>
#include <map>
#include "person.h"
#include "person.cc"

using namespace std;

class bank {
public:
	bank();
	~bank();
	bool create_account(int id);
	bool delete_account(int id);
	bool save(int id, int cnt);
	bool take(int id, int cnt);
	bool query(int id);
	person p;
private:
	map<int, int> account;
	int account_num;
};


#endif