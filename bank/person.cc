#pragma once
#include "person.h"

person::person() {}
person::~person() {}

int person::getPersonType(int id) {
	if (people.find(id) == people.end()) {
		cout << "getPersonType failed" << '\n';
	}
	return people.at(id);
}

bool person::setPersonType(int id, int type) {
	people.insert(pair<int, int> (id, type));
	cout << "setPersonType success" << endl;
	return true;
}
