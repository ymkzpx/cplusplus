#pragma once
#include <map>
using namespace std;

class person {
public:
	person();
	~person();
	int getPersonType(int id);
	bool setPersonType(int id, int type);
private:
	map<int, int> people;
};