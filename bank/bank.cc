#include "bank.h"

bank::bank() {}

bank::~bank() {}

bool bank::create_account(int id) {
	if (account.find(id) != account.end()) {
		cout << "create account failed" << "\n";
		return false;
	}
	account.insert(pair<int, int>(1, 0));
	cout << "create account success" << "\n";
	return true;
}

bool bank::delete_account(int id) {
	if (account.find(id) == account.end()) {
		cout << "delete account failed" << "\n";
		return false;
	}
	account.erase(id);
	cout << "delete account success" << "\n";
	return true;
}

bool bank::save(int id, int cnt) {
	if (account.find(id) == account.end()) {
		cout << "account not exist, please create account" << "\n";
		return false;
	}
	account.at(id) += cnt;
	cout << "save success" << "\n";
	return true;
}

bool bank::take(int id, int cnt) {
	if (account.find(id) == account.end()) {
		cout << "account not exist" << "\n";
		return false;
	}
	if (cnt > account.at(id)) {
		cout << "account res not enough" << "\n";
		return false;
	}
	account.at(id) -= cnt;
	cout << "take success" << "\n";
	return true;
}

bool bank::query(int id) {
	if (account.find(id) == account.end()) {
		cout << "query account not exist" << "\n";
		return false;
	}
	cout << account.at(id) << '\n';
	return true;
}